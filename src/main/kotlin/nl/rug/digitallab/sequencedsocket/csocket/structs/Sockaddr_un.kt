package nl.rug.digitallab.sequencedsocket.csocket.structs

import com.sun.jna.Structure
import com.sun.jna.Structure.FieldOrder

/**
 * Represents a socket address.
 */
@FieldOrder("sun_family", "sun_path")
internal class Sockaddr_un: Structure() {
    /**
     * The address family of the socket.
     */
    @JvmField
    var sun_family: Short = 0

    /**
     * The address data of the socket.
     */
    @JvmField
    var sun_path: ByteArray = ByteArray(108)

    /**
     * Loads a path into the socket address.
     *
     * @param path The path to load into the socket address.
     *
     * @throws IllegalArgumentException If the path is too long. The path can only be 107 characters long.
     */
    fun loadPath(path: String) {
        if (path.length > 107) throw IllegalArgumentException("Path is too long. Can only be 107 characters long.")

        // Preserve the size of the path.
        for (i in path.indices) {
            sun_path[i] = path[i].code.toByte()
        }
        // CharArray is already initialized with null characters.
    }
}