package nl.rug.digitallab.sequencedsocket.csocket.typedefs

/**
 * Address family options for sockets.
 *
 * @param value The value that the option represents.
 */
internal enum class AddressFamily(val value: Int) {
    AF_UNIX(1),
}
