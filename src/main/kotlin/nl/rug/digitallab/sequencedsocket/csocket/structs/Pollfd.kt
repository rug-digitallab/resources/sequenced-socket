package nl.rug.digitallab.sequencedsocket.csocket.structs

import com.sun.jna.Structure
import com.sun.jna.Structure.FieldOrder
import nl.rug.digitallab.sequencedsocket.csocket.typedefs.Polling

/**
 * Represents a pollfd struct used by the C socket library for polling.
 */
@FieldOrder("fd", "events", "revents")
internal class Pollfd: Structure() {
    /**
     * The file descriptor to poll.
     */
    @JvmField
    var fd: Int = 0

    /**
     * The events to poll for.
     */
    @JvmField
    var events: Short = 0

    /**
     * The events that occurred.
     */
    @JvmField
    var revents: Short = 0

    companion object {
        /**
         * Creates a new pollfd struct from a file descriptor.
         * Assumed the basic polling options used in this library.
         * This is the same as setting the events to POLLIN.
         *
         * @param fd The file descriptor to create the pollfd struct from.
         *
         * @return The pollfd struct.
         */
        fun createFromFd(fd: Int): Pollfd {
            val pollfd = Pollfd()
            pollfd.fd = fd
            pollfd.events = Polling.POLLIN.value
            return pollfd
        }
    }
}
