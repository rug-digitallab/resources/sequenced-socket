package nl.rug.digitallab.sequencedsocket.csocket

import com.sun.jna.LastErrorException
import com.sun.jna.Library
import com.sun.jna.Native

/**
 * Contains native C functions that are used to create and manage sockets, but aren't part of sys/socket.h
 */
internal interface CAuxiliary: Library {
    /**
     * Closes a file descriptor.
     *
     * @param socket The file descriptor to close.
     *
     * @return 0 if the file descriptor was closed successfully, -1 otherwise.
     */
    @Throws(LastErrorException::class)
    fun close(socket: Int): Int

    companion object {
        /**
         * The instance of the auxiliary library.
         * This should be used to call the functions in the library.
         */
        val INSTANCE = Native.load("c", CAuxiliary::class.java)
    }
}