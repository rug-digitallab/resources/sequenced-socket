package nl.rug.digitallab.sequencedsocket.csocket.typedefs

/**
 * Socket type options for sockets.
 *
 * @param value The value that the option represents.
 */
internal enum class SocketType(val value: Int) {
    SOCK_SEQPACKET(5),
}
