package nl.rug.digitallab.sequencedsocket.csocket.typedefs

/**
 * Polling options for sockets.
 *
 * @param value The value that the option represents.
 */
internal enum class Polling(val value: Short) {
    POLLIN(1),
    POLLERR(8),
    POLLNVAL(32),
}
