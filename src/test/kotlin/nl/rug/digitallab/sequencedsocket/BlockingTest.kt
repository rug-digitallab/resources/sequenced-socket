package nl.rug.digitallab.sequencedsocket

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

/**
 * Tests that blocking calls can be cancelled.
 */
class BlockingTest {
    /**
     * Tests that accept can be cancelled.
     */
    @Test
    fun `Tests that accept can be cancelled`() {
        val server = DefaultSequencedServer()
        server.start("/tmp/test.sock")

        runBlocking {
            val accept = async(Dispatchers.IO) { server.accept() }

            accept.cancelAndJoin()
        }
    }

    /**
     * Tests that read can be cancelled.
     */
    @Test
    fun `Tests that read can be cancelled`() {
        val server = DefaultSequencedServer()
        server.start("/tmp/test.sock")

        runBlocking {
            val accept = async(Dispatchers.IO) { server.accept() }
            val client = async(Dispatchers.IO) { DefaultSequencedClient().connect("/tmp/test.sock") }

            val clientSocket = accept.await()
            val read = async(Dispatchers.IO) { clientSocket.read(1024) }

            client.await()
            read.cancelAndJoin()
        }
    }
}
