# Sequenced Socket
Sequenced Socket is a Kotlin / Java / JVM library for creating sockets that guarantee the order of messages.
It implements the sequenced socket (ssocket or SOCK_SEQPACKET) to be used in the JVM.
It does this by integrating with the C programming language's sys/socket.h library through JNA.

## Authors
### Organisation
University of Groningen

### Development team
Digital lab

### Current maintainer
Lars Andringa

## Architectural decisions
### JNI vs JNA vs JNR
The first decision to make was whether to use JNI, JNA or JNR.
JNI is the oldest of the three. It is very efficient, but is difficult to use and has a lot of boilerplate code. Compiling it is also complicated, as it uses header files.
JNA is a lot less efficient, but is easier to use and has less boilerplate code. It also doesn't use header files, so it is easier to compile.
JNR is the newest. It is supposed to be between the two previous options, being both easy to use and efficient. But it often lacks documentation and it is not as widely used as the other two.

After having looked at all 3, we decided to go for JNA. While there is a performance tradeoff, it is simply by far the cleanest option in terms of code, integrating very nicely with the Kotlin code.
There is no non-Kotlin code involved at all, which makes it very easy to maintain.

We may look at JNR in the future, but currently it is simply not clear how to exactly implement this in JNR, which means JNA is the best option for now.

JNI is not an option. There is simply too much involved when we are talking about header files and manually needing to implement a lot of the C integration.

## Usecases
- Sequenced socket communication.
- gVisor runtime monitoring, which uses sequenced sockets.