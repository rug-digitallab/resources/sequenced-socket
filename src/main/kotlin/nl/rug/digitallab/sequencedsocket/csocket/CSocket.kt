package nl.rug.digitallab.sequencedsocket.csocket

import com.sun.jna.LastErrorException
import com.sun.jna.Library
import com.sun.jna.Native
import nl.rug.digitallab.sequencedsocket.csocket.structs.Pollfd
import nl.rug.digitallab.sequencedsocket.csocket.structs.Sockaddr_un

/**
 * Implements the C socket library into Kotlin.
 */
internal interface CSocket: Library {
    /**
     * Creates a new socket.
     *
     * @param domain The domain of the socket.
     * @param type The type of the socket.
     * @param protocol The protocol of the socket.
     *
     * @return The socket that was created.
     */
    @Throws(LastErrorException::class)
    fun socket(domain: Int, type: Int, protocol: Int): Int

    /**
     * Connects a socket to an address.
     *
     * @param socket The socket to connect.
     * @param address The address to connect to.
     * @param address_len The length of the address.
     *
     * @return 0 if the socket was successfully connected, -1 otherwise.
     */
    @Throws(LastErrorException::class)
    fun bind(socket: Int, address: Sockaddr_un, address_len: Int): Int

    /**
     * Accepts a connection on a socket.
     *
     * @param socket The server socket to accept the connection on.
     * @param address The address of the connection.
     * @param address_len The length of the address.
     *
     * @return The socket that the connection was accepted on.
     */
    @Throws(LastErrorException::class)
    fun accept(socket: Int, address: Sockaddr_un?, address_len: Int?): Int

    /**
     * Listens for connections on a socket.
     *
     * @param socket The socket to listen on.
     * @param backlog The maximum length of the queue of pending connections.
     *
     * @return 0 if the socket was successfully set to listen, -1 otherwise.
     */
    @Throws(LastErrorException::class)
    fun listen(socket: Int, backlog: Int): Int

    /**
     * Reads a message from a socket.
     *
     * @param socket The socket to read from.
     * @param buffer The buffer to read the message into.
     * @param buffer_size The size of the buffer.
     *
     * @return The amount of bytes that were read.
     */
    @Throws(LastErrorException::class)
    fun read(socket: Int, buffer: ByteArray, buffer_size: Int): Int

    /**
     * Connects a socket to an address.
     *
     * @param socket The socket to connect.
     * @param address The address to connect to.
     * @param address_len The length of the address.
     *
     * @return 0 if the socket was successfully connected, -1 otherwise.
     */
    @Throws(LastErrorException::class)
    fun connect(socket: Int, address: Sockaddr_un, address_len: Int): Int

    /**
     * Sends a message to a socket.
     *
     * @param socket The socket to send the message to.
     * @param buffer The buffer to send the message from.
     * @param buffer_size The size of the buffer.
     * @param flags The flags to send the message with.
     *
     * @return The amount of bytes that were sent.
     */
    @Throws(LastErrorException::class)
    fun send(socket: Int, buffer: ByteArray, buffer_size: Int, flags: Int): Int

    /**
     * Polls a socket for events.
     *
     * @param fds The file descriptor to poll.
     * @param nfds The events to poll for.
     * @param timeout The events that occurred.
     */
    @Throws(LastErrorException::class)
    fun poll(fds: Array<Pollfd>, nfds: Int, timeout: Int): Int

    companion object {
        /**
         * The instance of the C socket library.
         * This should be used to call the functions in the library.
         */
        val INSTANCE = Native.load("c", CSocket::class.java)
    }
}
