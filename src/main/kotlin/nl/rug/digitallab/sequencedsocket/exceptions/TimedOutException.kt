package nl.rug.digitallab.sequencedsocket.exceptions

import java.io.IOException

/**
 * Exception thrown when a call times out.
 *
 * @param message The message to display with the exception.
 */
class TimedOutException(message: String): IOException(message)
