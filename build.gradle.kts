plugins {
    id("nl.rug.digitallab.gradle.plugin.kotlin.library")
}

dependencies {
    val jnaVersion: String by project
    val coroutinesVersion: String by project
    val smallRyeConfigVersion: String by project
    val jakartaAnnotationsVersion: String by project

    implementation("net.java.dev.jna:jna-platform:$jnaVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
    implementation("io.smallrye.config:smallrye-config:$smallRyeConfigVersion")
    implementation("jakarta.annotation:jakarta.annotation-api:$jakartaAnnotationsVersion")

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}
