package nl.rug.digitallab.sequencedsocket.interfaces

/**
 * Represents a client socket that is connected to a server socket.
 */
interface SequencedClient : AutoCloseable {
    /**
     * Reads a message from the client socket.
     *
     * @param readSize The size of the buffer that the message will be read into.
     * @param timeout The amount of time to wait for a message to be read in ms. If 0, the call will block until a message is read.
     *
     * @return The message that was read from the client socket.
     */
    suspend fun read(readSize: Int, timeout: Int = 0): ByteArray

    /**
     * Writes a message to the connected socket.
     *
     * @param message The message to write to the socket.
     *
     * @return The number of bytes that were written.
     */
    fun write(message: ByteArray): Int

    /**
     * Connects the client socket to a listening socket.
     *
     * @param path The path of the socket to connect to.
     *
     * @return The client socket that was connected.
     */
    fun connect(path: String): Int

    /**
     * Closes the client socket.
     */
    override fun close()
}
