package nl.rug.digitallab.sequencedsocket

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.sequencedsocket.exceptions.TimedOutException
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

/**
 * Tests that the timeout versions of the blocking calls work.
 */
class TimeoutTest {
    /**
     * Tests that accept can time out.
     */
    @Test
    fun `Tests that accept can time out`() {
        val server = DefaultSequencedServer()
        server.start("/tmp/test.sock")

        assertThrows<TimedOutException> { runBlocking { server.accept(1000) } }
    }

    /**
     * Tests that read can time out.
     */
    @Test
    fun `Tests that read can time out`() {
        val server = DefaultSequencedServer()
        server.start("/tmp/test.sock")

        runBlocking {
            val accept = async(Dispatchers.IO) { server.accept() }
            val client = async(Dispatchers.IO) { DefaultSequencedClient().connect("/tmp/test.sock") }

            val clientSocket = accept.await()

            client.await()
            assertThrows<TimedOutException> { runBlocking { clientSocket.read(1024, 1000) } }
        }
    }
}
