package nl.rug.digitallab.sequencedsocket

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

/**
 * Integration test for a server sending a message to a client.
 */
class ServerToClient {
    /**
     * Full integration test of a server sending a message to a client.
     */
    @Test
    fun `Server sending a message to the client`() {
        runBlocking {
            val server = async(Dispatchers.IO) { server() }
            val client = async(Dispatchers.IO) { client() }

            server.await()
            client.await()
        }
    }

    /**
     * The client side of the integration test.
     */
    private suspend fun client() {
        delay(1000)
        val clientSocket = DefaultSequencedClient()
        clientSocket.connect("/tmp/test.sock")
        val message = clientSocket.read(1024)
        assertEquals("Hello, world!", String(message))
        clientSocket.close()
    }

    /**
     * The server side of the integration test.
     */
    private suspend fun server() {
        val serverSocket = DefaultSequencedServer()
        serverSocket.start("/tmp/test.sock")
        val clientSocket = serverSocket.accept()
        clientSocket.write("Hello, world!".toByteArray())

        clientSocket.close()
        serverSocket.close()
    }
}
