rootProject.name = "sequenced-socket"

pluginManagement {
    plugins {
        val digitalLabGradlePluginVersion: String by settings

        id("nl.rug.digitallab.gradle.plugin.kotlin.library") version digitalLabGradlePluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven")
        gradlePluginPortal()
    }
}
