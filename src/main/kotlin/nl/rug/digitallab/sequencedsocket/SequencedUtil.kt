package nl.rug.digitallab.sequencedsocket

import kotlinx.coroutines.yield
import nl.rug.digitallab.sequencedsocket.csocket.CSocket
import nl.rug.digitallab.sequencedsocket.csocket.structs.Pollfd
import nl.rug.digitallab.sequencedsocket.csocket.typedefs.Polling
import nl.rug.digitallab.sequencedsocket.exceptions.TimedOutException
import org.eclipse.microprofile.config.ConfigProvider
import java.io.IOException
import kotlin.experimental.and

/**
 * Util functions to help the client and server.
 */
internal object SequencedUtil {
    val pollFrequency = ConfigProvider.getConfig().getValue("sequencedsocket.pollfrequency", Int::class.java)

    /**
     * Polls the socket until it is ready to be accepted.
     *
     * @param socket The socket to poll.
     *
     * @throws IOException When an exception is encountered while polling the socket.
     */
    suspend fun poll(socket: Int) {
        val pollAddress = Pollfd.createFromFd(socket)

        do {
            val pollResult = CSocket.INSTANCE.poll(arrayOf(pollAddress), 1, pollFrequency)
            yield()

            // Should have thrown a LastErrorException already, but just as an extra safety measure.
            if (pollResult == -1) throw IOException("Failed to poll the socket.")
            // If the result is 1, the socket is ready to be accepted.
        } while (pollResult != 1)

        if (pollAddress.revents and Polling.POLLERR.value != 0.toShort()) {
            throw IOException("Failed to poll the socket.")
        }
        if (pollAddress.revents and Polling.POLLNVAL.value != 0.toShort()) {
            throw IOException("Invalid request: The socket does not refer to any open file.")
        }
    }

    /**
     * Polls the socket until it is ready to be accepted, or until the timeout is reached.
     *
     * @param socket The socket to poll.
     * @param timeout The timeout in ms.
     *
     * @throws IOException When an exception is encountered while polling the socket.
     * @throws TimedOutException When the timeout is reached.
     */
    suspend fun pollWithTimeout(socket: Int, timeout: Int) {
        val pollAddress = Pollfd.createFromFd(socket)

        for (i in 0 until timeout / pollFrequency) {
            val pollResult = CSocket.INSTANCE.poll(arrayOf(pollAddress), 1, pollFrequency)
            yield()

            // Should have thrown a LastErrorException already, but just as an extra safety measure.
            if (pollResult == -1) throw IOException("Failed to poll the socket.")
            // If the result is 1, the socket is ready to be accepted.
            if (pollResult == 1) {
                return
            }
        }

        throw TimedOutException("Timed out while waiting for a client socket to connect.")
    }
}
