package nl.rug.digitallab.sequencedsocket

import nl.rug.digitallab.sequencedsocket.csocket.CAuxiliary
import nl.rug.digitallab.sequencedsocket.csocket.CSocket
import nl.rug.digitallab.sequencedsocket.csocket.structs.Sockaddr_un
import nl.rug.digitallab.sequencedsocket.csocket.typedefs.AddressFamily
import nl.rug.digitallab.sequencedsocket.csocket.typedefs.SocketType
import nl.rug.digitallab.sequencedsocket.interfaces.SequencedClient
import nl.rug.digitallab.sequencedsocket.interfaces.SequencedServer
import java.io.IOException
import java.nio.file.Path
import kotlin.io.path.deleteIfExists
import kotlin.properties.Delegates

/**
 * Represents a server socket that can accept client sockets.
 */
open class DefaultSequencedServer: SequencedServer {
    private var serverSocket by Delegates.notNull<Int>()

    /**
     * Creates a new server socket.
     *
     * @throws IOException When an exception is encountered while creating the socket.
     */
    init {
        convertExceptions {
            serverSocket = CSocket.INSTANCE.socket(AddressFamily.AF_UNIX.value, SocketType.SOCK_SEQPACKET.value, 0)
        }
    }

    /**
     * Starts the server socket.
     *
     * @param path The filesystem path of the unix domain socket.
     *
     * @throws IOException When an exception is encountered while starting the server socket.
     */
    @Throws(IOException::class)
    override fun start(path: String) = convertExceptions {
        // Delete the socket file if it already exists.
        Path.of(path).deleteIfExists()

        // Create the address of the socket.
        val address = Sockaddr_un()
        address.sun_family = AddressFamily.AF_UNIX.value.toShort()
        address.loadPath(path)

        // Bind the socket to the address.
        CSocket.INSTANCE.bind(serverSocket, address, 110)

        // Start listening for connections.
        CSocket.INSTANCE.listen(serverSocket, 5)
    }

    /**
     * Accepts a client socket trying to connect to the server socket.
     *
     * @param timeout The amount of time to wait for a client to connect in ms. If 0, the call will block until a client connects.
     *
     * @return The client socket that was accepted.
     *
     * @throws IOException When an exception is encountered while accepting the client socket.
     */
    @Throws(IOException::class)
    override suspend fun accept(timeout: Int): SequencedClient = convertExceptionsSuspend {
        if (timeout == 0) {
            SequencedUtil.poll(serverSocket)
        } else {
            SequencedUtil.pollWithTimeout(serverSocket, timeout)
        }

        val clientSocket = CSocket.INSTANCE.accept(serverSocket, null, null)
        return@convertExceptionsSuspend DefaultSequencedClient(clientSocket)
    }

    /**
     * Closes the server socket.
     *
     * @throws IOException When an exception is encountered while closing the server socket.
     */
    @Throws(IOException::class)
    override fun close(): Unit = convertExceptions {
        CAuxiliary.INSTANCE.close(serverSocket)
    }
}
