package nl.rug.digitallab.sequencedsocket

import com.sun.jna.LastErrorException
import java.io.IOException

/**
 * Converts exceptions thrown by the C socket library to exceptions thrown by the Kotlin socket library.
 * This is done by converting the LastErrorException to an IOException, and removing the error code from the message.
 *
 * @param block The block of code to execute.
 */
internal fun <T> convertExceptions(
    block: () -> T
): T {
    try {
        return block()
    } catch (e: LastErrorException) {
        throw IOException(e.message, e)
    }
}

/**
 * Converts exceptions thrown by the C socket library to exceptions thrown by the Kotlin socket library.
 * This is done by converting the LastErrorException to an IOException, and removing the error code from the message.
 *
 * @param block The block of code to execute.
 */
internal suspend fun <T> convertExceptionsSuspend(
    block: suspend () -> T
): T {
    try {
        return block()
    } catch (e: LastErrorException) {
        throw IOException(e.message, e)
    }
}
