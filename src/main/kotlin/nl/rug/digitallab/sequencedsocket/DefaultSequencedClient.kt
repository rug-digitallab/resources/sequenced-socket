package nl.rug.digitallab.sequencedsocket

import nl.rug.digitallab.sequencedsocket.csocket.CAuxiliary
import nl.rug.digitallab.sequencedsocket.csocket.CSocket
import nl.rug.digitallab.sequencedsocket.csocket.structs.Sockaddr_un
import nl.rug.digitallab.sequencedsocket.csocket.typedefs.AddressFamily
import nl.rug.digitallab.sequencedsocket.csocket.typedefs.SocketType
import nl.rug.digitallab.sequencedsocket.exceptions.TimedOutException
import nl.rug.digitallab.sequencedsocket.interfaces.SequencedClient
import java.io.IOException

/**
 * Represents a client socket that is connected to a server socket.
 */
open class DefaultSequencedClient(
    private val clientSocket: Int
): SequencedClient {
    /**
     * If no client socket is provided, a new client socket is created.
     *
     * @throws IOException When an exception is encountered while creating the client socket.
     */
    @Throws(IOException::class)
    constructor() :
            this(convertExceptions{
                CSocket.INSTANCE.socket(AddressFamily.AF_UNIX.value, SocketType.SOCK_SEQPACKET.value, 0)
            })

    /**
     * Closes the client socket.
     *
     * @throws IOException When the C function sets an error code.
     */
    @Throws(IOException::class)
    override fun close(): Unit = convertExceptions {
        CAuxiliary.INSTANCE.close(clientSocket)
    }

    /**
     * Reads a message from the client socket. This function also has a timeout to allow for a maximum wait time.
     *
     * @param readSize The size of the buffer that the message will be read into.
     * @param timeout The maximum amount of time to wait for a message to arrive in ms. If the timeout is 0, the client socket will wait until cancelled.
     *
     * @return The message that was read from the client socket.
     *
     * @throws IOException When an exception is encountered while reading the message.
     * @throws TimedOutException When the timeout is reached.
     */
    @Throws(IOException::class, TimedOutException::class)
    override suspend fun read(readSize: Int, timeout: Int): ByteArray = convertExceptionsSuspend {
        if (timeout == 0) {
            SequencedUtil.poll(clientSocket)
        } else {
            SequencedUtil.pollWithTimeout(clientSocket, timeout)
        }

        val buffer = ByteArray(readSize)
        val read = CSocket.INSTANCE.read(clientSocket, buffer, buffer.size)
        return@convertExceptionsSuspend buffer.copyOf(read)
    }

    /**
     * Writes a message to the connected socket.
     *
     * @param message The message to write to the socket.
     *
     * @throws IOException When an exception is encountered while writing the message.
     */
    @Throws(IOException::class)
    override fun write(message: ByteArray) = convertExceptions {
        CSocket.INSTANCE.send(clientSocket, message, message.size, 0)
    }

    /**
     * Connects the client socket to a listening socket.
     *
     * @param path The path of the socket to connect to.
     *
     * @throws IOException When an exception is encountered while connecting the client socket.
     */
    @Throws(IOException::class)
    override fun connect(path: String) = convertExceptions {
        val address = Sockaddr_un()
        address.sun_family = AddressFamily.AF_UNIX.value.toShort()
        address.loadPath(path)

        CSocket.INSTANCE.connect(clientSocket, address, 110)
    }
}
