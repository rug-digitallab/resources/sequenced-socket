package nl.rug.digitallab.sequencedsocket.interfaces

/**
 * Represents a server socket that can accept client sockets.
 */
interface SequencedServer : AutoCloseable {

    /**
     * Starts the server socket.
     *
     * @param path The filesystem path of the unix domain socket.
     *
     * @return The server socket that was started.
     */
    fun start(path: String): Int

    /**
     * Accepts a client socket trying to connect to the server socket.
     *
     * @param timeout The amount of time to wait for a client to connect in ms. If 0, the call will block until a client connects.
     *
     * @return The client socket that was accepted.
     */
    suspend fun accept(timeout: Int = 0): SequencedClient

    /**
     * Closes the server socket.
     */
    override fun close()
}
