package nl.rug.digitallab.sequencedsocket.integration

import com.sun.jna.LastErrorException
import nl.rug.digitallab.sequencedsocket.convertExceptions
import nl.rug.digitallab.sequencedsocket.csocket.CSocket
import nl.rug.digitallab.sequencedsocket.csocket.structs.Sockaddr_un
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.IOException
import kotlin.test.assertEquals

/**
 * This class tests that there is a proper exception handling system for the JNA integration.
 */
class ExceptionHandling {
    /**
     * Tests that the bind function throws an exception with a bad protocol.
     */
    @Test
    fun `Tests that the bind function throws an exception with a bad protocol`() {
        val address = Sockaddr_un()
        address.sun_family = 100 // Bad protocol.
        address.loadPath("test.sock")
        assertThrows<LastErrorException> { CSocket.INSTANCE.bind(0, address, 0) }
    }

    /**
     * Tests the processing of the LastErrorException
     */
    @Test
    fun `Tests the processing of the LastErrorException`() {
        // Generate error
        val address = Sockaddr_un()
        address.sun_family = 100 // Bad protocol.
        address.loadPath("test.sock")
        assertThrows<IOException> {
            convertExceptions { CSocket.INSTANCE.bind(0, address, 0) }
        }
    }
}
