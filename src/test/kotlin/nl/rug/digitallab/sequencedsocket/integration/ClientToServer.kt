package nl.rug.digitallab.sequencedsocket.integration

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.sequencedsocket.DefaultSequencedClient
import nl.rug.digitallab.sequencedsocket.DefaultSequencedServer
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

/**
 * Integration test for a client sending a message to a server.
 */
class ClientToServer {
    /**
     * Full integration test of a client sending a message to a server.
     */
    @Test
    fun `Client sending a message to the server`() {
        runBlocking {
            val server = async(Dispatchers.IO) { server() }
            val client = async(Dispatchers.IO) { client() }

            server.await()
            client.await()
        }
    }

    /**
     * The client side of the integration test.
     */
    private suspend fun client() {
        delay(1000)
        val clientSocket = DefaultSequencedClient()
        clientSocket.connect("/tmp/test.sock")
        clientSocket.write("Hello, world!".toByteArray())
        clientSocket.close()
    }

    /**
     * The server side of the integration test.
     */
    private suspend fun server() {
        val serverSocket = DefaultSequencedServer()
        serverSocket.start("/tmp/test.sock")
        val clientSocket = serverSocket.accept()
        val message = clientSocket.read(1024)
        assertEquals("Hello, world!", String(message))

        clientSocket.close()
        serverSocket.close()
    }
}
